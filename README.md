# Simple Fake Live Chat Server

Simple WebSocket server to send lots of fake chat messages on connect.
You can also send a message and it will be broadcasted to clients.

## Starting the app

To start the app run `npm start` after you have installed dependencies via `npm install`.
It will start HTTP server on port `8080`. You can change the port by setting `PORT` environment variable to the command.

## Testing with wscat

You can use `wscat` (or any other tool of your choice) to test the websocket server. Run `npm install -g wscat` to install it.

Example output:

```shell
$ wscat --connect ws://localhost:8080
Connected (press CTRL+C to quit)
< {"fullName":"Camille Hansen","userName":"Emerald.OKeefe82","text":"Quae ab voluptas labore.\nTempore cupiditate voluptatibus.\nItaque odit quasi porro consequatur temporibus labore.\nFugiat repudiandae distinctio libero deserunt."}
< {"fullName":"Gina Rosenbaum IV","userName":"Garret5","text":"Sequi molestiae amet. Praesentium reprehenderit dolores molestias recusandae nesciunt itaque nihil minima reiciendis. Eum voluptas eligendi adipisci deleniti quae. Adipisci inventore nemo in."}
< {"fullName":"Merle Jones IV","userName":"Ana_Wunsch","text":"Delectus explicabo laudantium voluptatibus cum."}
< {"fullName":"Linda McCullough","userName":"Jaylon.Hirthe","text":"pariatur"}
< {"fullName":"Ms. Mitchell Hilpert","userName":"Alaina_Greenholt","text":"temporibus enim suscipit"}
< {"fullName":"Carolyn Kub","userName":"Ida_Kuvalis","text":"sequi"}
> { "fullName": "Orkhan Huseynli", "userName": "0xeac7", "text": "What the hell?!" }
< { "fullName": "Orkhan Huseynli", "userName": "0xeac7", "text": "What the hell?!" }

```
