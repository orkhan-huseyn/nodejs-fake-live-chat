import { createServer } from 'http'
import { WebSocketServer, WebSocket } from 'ws'
import { newChatMessage, newMessage } from './util.js'

const port = process.env.PORT || 8080

const server = createServer()
const wss = new WebSocketServer({ server })

wss.on('connection', function (client) {
  startBroadCastStream(client)
  client.on('message', broadCastMessage)
})

function startBroadCastStream(client) {
  setInterval(function () {
    const message = newMessage()
    client.send(JSON.stringify(message))
  }, 1000)
}

function broadCastMessage(message) {
  wss.clients.forEach(function each(client) {
    if (client.readyState === WebSocket.OPEN) {
      const msg = newChatMessage(message.toString())
      client.send(JSON.stringify(msg))
    }
  })
}

server.listen(port, function () {
  console.log('HTTP Server is running on port ' + port)
})
