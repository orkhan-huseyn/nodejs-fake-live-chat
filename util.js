import { faker } from '@faker-js/faker'

const MessageType = {
  CHAT_MESSAGE: 'chat-message',
  JOIN: 'join',
  BAN: 'ban',
}

const spec = {
  [MessageType.CHAT_MESSAGE]: 0.7,
  [MessageType.JOIN]: 0.2,
  [MessageType.BAN]: 0.1,
}

export function newMessage() {
  const randomChoice = weightedRandom(spec)

  if (randomChoice === MessageType.CHAT_MESSAGE) {
    return newChatMessage()
  }

  if (randomChoice === MessageType.JOIN) {
    return newChatJoin()
  }

  return newUserBan()
}

function weightedRandom(weights) {
  const r = Math.random()
  let sum = 0
  for (const item in weights) {
    sum += weights[item]
    if (r <= sum) return item
  }
}

export function newChatMessage(defaultText = '') {
  const userName = faker.internet.userName()
  const text = defaultText || faker.lorem.text()
  return { type: MessageType.CHAT_MESSAGE, userName, text }
}

export function newChatJoin() {
  const userName = faker.internet.userName()
  return {
    type: MessageType.JOIN,
    userName,
  }
}

export function newUserBan() {
  const userName = faker.internet.userName()
  return {
    type: MessageType.BAN,
    userName,
  }
}
